// Firmware Version : MSD1.3   - 6 relay without CT and default relay ON


#include <SimpleModbusSlave_DUE.h>
#include <EEPROM.h>
#include <Timer.h>

//b17 think valley building first floor sector 32 gurgaon
//122001
//fuse bits     low:FF , high:DA , ext:05


#define FIRMWARE            14          //1.4 (new 6 relay board)
#define HARDWARE            13          //MSD Relay V1.3
#define MODBUS_ID_LOC       1
#define MODBUS_CONFIG_LOC   4
#define DEFAULT_STATE_LOC0  6
#define DEFAULT_STATE_LOC1  7
#define MODBUS_OK           1
#define MODBUS_DEFAULT_ID   1


#define HIGH_   (1)
#define LOW_    (0)
//#define 
       
#define RELAY1  (8)
#define RELAY2  (7)
#define RELAY3  (3)
#define RELAY4  (5)
#define RELAY5  (A1)
#define RELAY6  (4)

#define TX_EN   (2)
#define TEMP_RST (9)
#define TEMP    (10)

//////////////// registers of your slave ///////////////////
enum 
{     
  // just add or remove registers and your good to go...
  // The first register starts at address 0
  MB_0_R,     
  MB_1_R,
  MB_2_R,
  MB_3_R,        
  MB_4_R,     
  MB_5_R,
  MB_6_R,
  MB_7_R,        
  MB_8_R,         
  MB_9_T,
  MB_10_RE,
  MB_11_L,
  MB_12_B,
  MB_13_FW,             // Firmware version
  MB_14_HW,             // hardware version
  MB_15_DF,             //default state
  MB_16_REBOOTCNT,
  MB_17_MODBUSID,
  MB_18_THRESHOLD,
  MB_19_HEARTBEAT,
  MB_20_CSD,/*CSD-> Current Sensor Digital Value*/
  MB_21_CSD,
  MB_22_CSD,
  MB_23_CSD,
  MB_24_CSD,
  MB_25_CSD,
  MB_26_CSD,
  MB_27_CSD,
  MB_28_CSD,
  MB_29_NOTUSED,
  MB_30_CSA,
  MB_31_CSA,/*CSA-> Current Sensor Analog Value*/
  MB_32_CSA,
  MB_33_CSA,
  MB_34_CSA,
  MB_35_CSA,
  MB_36_CSA,
  MB_37_CSA,
  MB_38_CSA,
  MB_39_TEMPRST,
  MB_40_TEMP_0_ADD,
  MB_41_TEMP_0_ADD,
  MB_42_TEMP_0_ADD,
  MB_43_TEMP_0_ADD,
  MB_44_TEMP_0_VAL,
  MB_40_TEMP_1_ADD,
  MB_41_TEMP_1_ADD,
  MB_42_TEMP_1_ADD,
  MB_43_TEMP_1_ADD,
  MB_44_TEMP_1_VAL,
  MB_40_TEMP_2_ADD,
  MB_41_TEMP_2_ADD,
  MB_42_TEMP_2_ADD,
  MB_43_TEMP_2_ADD,
  MB_44_TEMP_2_VAL,
  HOLDING_REGS_SIZE // leave this one
};

unsigned int holdingRegs[HOLDING_REGS_SIZE];
int reboot_val;
static unsigned int heartbeatCount=0;
static unsigned char modbusId=0;
unsigned char state;
//boolean led=true;

union Data_int{
  unsigned int value;
  char Byte[2];
}Dat;
/*Timer instance */
Timer tm;

void ConfigRelay(void){
  pinMode(RELAY1,OUTPUT);
  pinMode(RELAY2,OUTPUT);
  pinMode(RELAY3,OUTPUT);
  pinMode(RELAY4,OUTPUT);
  pinMode(RELAY5,OUTPUT);
  pinMode(RELAY5,OUTPUT);
  pinMode(RELAY6,OUTPUT);
}

void ProcessRelay(void){
  if(holdingRegs[MB_0_R]){
    digitalWrite(RELAY1,HIGH_);
  }
  else{
    digitalWrite(RELAY1,LOW_);
  } 
  if(holdingRegs[MB_1_R]){
    digitalWrite(RELAY2,HIGH_);
  }
  else{
    digitalWrite(RELAY2,LOW_);
  }
  if(holdingRegs[MB_2_R]){
    digitalWrite(RELAY3,HIGH_);
  }
  else{
    digitalWrite(RELAY3,LOW_);
  }
  if(holdingRegs[MB_3_R]){
    digitalWrite(RELAY4,HIGH_);
  }
  else{
    digitalWrite(RELAY4,LOW_);
  }
  if(holdingRegs[MB_4_R]){
    digitalWrite(RELAY5,HIGH_);
  }
  else{
    digitalWrite(RELAY5,LOW_);
  }
  if(holdingRegs[MB_5_R]){
    digitalWrite(RELAY6,HIGH_);
  }
  else{
    digitalWrite(RELAY6,LOW_);
  }
}

void HeartBeatEvent(void){
  heartbeatCount++;
  heartbeatCount&=0xFFFF;
  holdingRegs[MB_19_HEARTBEAT]=heartbeatCount; 
  //led = !led;
  //digitalWrite(LED,led);
}

void UpdateRebootCount(void){
  /*Get old reboot value*/
  reboot_val = (int)EEPROM.read(10);
  /*If first time or memory full condition occures then*/
  if (reboot_val==0xFF){
    /*reset the counter value*/
    reboot_val = 0;
  }
  else{
    /* inc by one*/
    reboot_val+=1;
  }
  /*Save the value in EEPROM*/
  EEPROM.write(10,reboot_val);
  //EEPROM.commit();
  /* Update the corresponding Holding register*/
  holdingRegs[MB_16_REBOOTCNT] = reboot_val;

}

void SetModbusId(unsigned char id){
  EEPROM.write(MODBUS_ID_LOC,id);
  //holdingRegs[MB_17_MODBUSID] = modbusId;
}

void GetModbusId(void){
  unsigned char temp;
  temp =EEPROM.read(MODBUS_CONFIG_LOC);
  if(temp==MODBUS_OK){
     modbusId=EEPROM.read(MODBUS_ID_LOC);
     holdingRegs[MB_17_MODBUSID] = modbusId;
  }
  else{
    EEPROM.write(MODBUS_ID_LOC,MODBUS_DEFAULT_ID);
    EEPROM.write(MODBUS_CONFIG_LOC,MODBUS_OK);
    holdingRegs[MB_17_MODBUSID] = MODBUS_DEFAULT_ID;
    modbusId = MODBUS_DEFAULT_ID;
  }
}

void SetDefaultState(unsigned char st){
  //Dat.value = st;
  EEPROM.write(20,st);
  //EEPROM.write(21,Dat.Byte[1]);
}
void GetDefaultState(void){
  //Dat.Byte[0] = EEPROM.read(20);
 // Dat.Byte[1] = EEPROM.read(21);
  state = EEPROM.read(20);
  if(state==255)
  state= 1;
  holdingRegs[MB_15_DF] = state;
}


void setup(){
  /*set all registers to ones*/
  memset(holdingRegs,0,HOLDING_REGS_SIZE);
  
  UpdateRebootCount();
  /*Configure the timer for heartbeat counter*/
  tm.every(1000,HeartBeatEvent);
  /*Get the stored modbus id*/
  GetModbusId();
  GetDefaultState();
  Serial.print(modbusId,DEC);
  /*parameters(SerialPort,baudrate, ID, tx_en,Holding_reg_size,Holding_reg_addr)*/
  modbus_configure(&Serial, 9600,MODBUS_DEFAULT_ID,TX_EN,HOLDING_REGS_SIZE, holdingRegs);
  modbus_update_comms(9600,modbusId);
  ConfigRelay();
  if(state == 0){
    holdingRegs[MB_0_R] = 0;
    holdingRegs[MB_1_R] = 0;
    holdingRegs[MB_2_R] = 0;
    holdingRegs[MB_3_R] = 0;
    holdingRegs[MB_4_R] = 0;
    holdingRegs[MB_5_R] = 0;
    holdingRegs[MB_6_R] = 0;
    holdingRegs[MB_7_R] = 0;
  }
  else{
    holdingRegs[MB_0_R] = 1;
    holdingRegs[MB_1_R] = 1;
    holdingRegs[MB_2_R] = 1;
    holdingRegs[MB_3_R] = 1;
    holdingRegs[MB_4_R] = 1;
    holdingRegs[MB_5_R] = 1;
    holdingRegs[MB_6_R] = 1;
    holdingRegs[MB_7_R] = 1;
  }
   holdingRegs[MB_13_FW] = FIRMWARE;
   holdingRegs[MB_14_HW] = HARDWARE;
}

void loop(){
  /*process relay if modbus holding register is modified*/
  ProcessRelay();
  if(state != holdingRegs[MB_15_DF])
  {
    state = holdingRegs[MB_15_DF];
    SetDefaultState(state);
  }
  if( holdingRegs[MB_17_MODBUSID]!=modbusId){
    SetModbusId((unsigned char)holdingRegs[MB_17_MODBUSID]);
    modbusId=holdingRegs[MB_17_MODBUSID];
    modbus_update_comms(9600,modbusId);
  }
  /*update the timer so that HeartBeatEvent can fire*/
  tm.update();
}

void serialEvent() {
   modbus_update_new();
}


